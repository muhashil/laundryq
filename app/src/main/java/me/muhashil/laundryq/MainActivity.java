package me.muhashil.laundryq;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Date;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    String[] listlaundry = {"Sayang Laundry", "Bale Laundry", "Pogung Laundry", "UGM Laundry"};
    Integer[] idoutlet = {51216, 34912, 59237, 20653};
    String[] listdate = {"18 Maret 2018", "27 Maret 2018", "3 April 2018", "10 April 2018"};
    Integer[] nopesanan = {11, 4, 15, 8};
    Integer[] imgdir = {R.drawable.s, R.drawable.b, R.drawable.p, R.drawable.u};


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ListView listRiwayat = (ListView) findViewById(R.id.riwayat_transaksi);
        customAdapter CustomAdapter = new customAdapter();
        listRiwayat.setAdapter(CustomAdapter);

        listRiwayat.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                startActivity(new Intent(MainActivity.this, detailLaundry.class));
            }
        });
    }

    class customAdapter extends BaseAdapter{

        @Override
        public int getCount() {
            return imgdir.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.customlayout, null);
            ImageView laundryImage = (ImageView) convertView.findViewById(R.id.imageView);
            TextView laundryName = (TextView) convertView.findViewById(R.id.textView_laundryName);
            TextView laundryDate = (TextView) convertView.findViewById(R.id.textView_laundryDate);

            laundryImage.setImageResource(imgdir[position]);
            laundryName.setText(listlaundry[position]);
            laundryDate.setText(listdate[position]);

            return convertView;
        }
    }
}
